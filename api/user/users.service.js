var Configurations = require('../../config/properties.js');
var Users = require('./users.dao');
const twilio = require('twilio')(Configurations.TWILIO_ACCOUNT_SID, Configurations.TWILIO_AUTH_TOKEN);
var request = require('request-promise');
exports.createUser = async (userData) => {
    try {
        let users = await Users.find({ $and: [{ $or: [{ email: userData.email }, { mobile: userData.mobile }] }, { isUserVerified: true }] })
        if (users.length) {
            let validationMessages = {
                email : '',
                mobile : '',
            }
            for (let user of users) {
                if (user.email == userData.email) {
                    validationMessages.email = 'Email ' + userData.email + ' already exists, Please enter new email and submit'
                }
                if (user.mobile == userData.mobile) {
                    validationMessages.mobile = 'Mobile number ' + userData.mobile + ' already exists, Please enter new mobile number and submit'
                }
            }
            return validationMessages
        }
        if (await checkifUserExist(userData)) {
            await updateUser(userData)
            return "User created successfully"
        }
        await Users.create(userData)
        return "User created successfully"
    } catch (err) {
        console.log(err)
        throw err;
    }
}

exports.isEmailVerified = async (email) => {
    try{
        let user = await Users.find({email:email,isEmailVerified: true})
        return ( user.length > 0)
    } catch(err) {
        console.log(err)
        throw err;
    }
}

exports.verifyCaptcha = async (req) => {
    try {
        var options = {
            method: 'POST',
            uri: 'https://www.google.com/recaptcha/api/siteverify',
            form: {
                secret: Configurations.CAPTCHA_SECRET_KEY,
                response: req.body['g-recaptcha-response'],
            },
            json: true
        };
        let respone = await request(options);
        return respone.success;
    } catch (err) {
        console.log(err);
        throw err;
    }
}

exports.checkIfValidPhoneNumber = (phoneNumber) => {
    return new Promise((resolve, reject) => {
        twilio.lookups.phoneNumbers(phoneNumber)
            .fetch()
            .then((phone_number) => {
                return resolve(true)
            })
            .catch((err) => {
                if (err.status == 404) {
                    return resolve(false)
                }
                console.log(err)
                return reject(err)
            });
    })
}

updateUser = async (userData) => {
    return new Promise((resolve, reject) => {
        Users.updateOne({ email: userData.email, mobile: userData.mobile }, {
            $set: {
                firstName: userData.firstName,
                lastName: userData.lastName,
                license: userData.license,
                clanName: userData.clanName
            }
        }, function (err, user) {
            if (err) {
                console.log(err)
                return reject(err)
            }
            return resolve('User updated')
        })
    })
}

checkifUserExist = async (userData) => {
    try {
        let user = await Users.find({ email: userData.email, mobile: userData.mobile })
        if (user.length) {
            return true
        }
        return false    
    } catch(err) {
        console.log(err)
        throw err;
    }
}

exports.createValidationMessages = (errors) => {
    let validationMessages = {
        firstName : '',
        lastName : '',
        email : '',
        mobile : '',
        mobileConfirm: '',
        emailConfirm: ''
    }
    for(let error of errors) {
        if(error.param == 'firstName' || error.param == 'lastName') {
            validationMessages[error.param] = 'Required'
        } else if (error.param == 'emailConfirm') {
            validationMessages[error.param] = 'Email not matched'
        } else if (error.param == 'mobileConfirm') {
            validationMessages[error.param] = 'Mobile not matched'
        } else {
            validationMessages[error.param] = error.param + ' ' + error.value + ' is invalid'
        }
    }
    return validationMessages
}

exports.sendOTP = async (sendTo, channelType) => {
    try {
        let verificationRequest;
        verificationRequest = await twilio.verify.services(Configurations.VERIFICATION_SID)
            .verifications
            .create({ to: sendTo, channel: channelType });
    } catch (err) {
        console.log(err);
        throw err;
    }
}

exports.modifyEmailVerification = async (email) => {
    return new Promise((resolve, reject) => {
        Users.updateOne({ email: email }, { $set: { isEmailVerified: true } }, function (err, user) {
            if (err) {
                console.log(err);
                return reject(err)
            }
            return resolve("Email Verified")
        })
    })
}

exports.verify = async (code, userMobileorEmail) => {
    try {
        let verificationResult;
        verificationResult = await twilio.verify.services(Configurations.VERIFICATION_SID)
            .verificationChecks
            .create({ code, to: userMobileorEmail });
        if (verificationResult.status === 'approved') {
            return "Verification successfull";
        }
        return "Enter correct verification code"
    } catch (e) {
        console.log(e);
        throw e;
    }
}

exports.modifyVerification = async (userData) => {
    return new Promise((resolve, reject) => {
        Users.updateOne({ email: userData.email, mobile: userData.mobile }, { $set: { isUserVerified: true } }, function (err, user) {
            if (err) {
                console.log(err);
                return reject(err)
            }
            return resolve("User Verified")
        })
    })
}
