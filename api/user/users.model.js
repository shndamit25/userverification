var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var usersSchema = new Schema({
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    mobile: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    license: {
        type: String
    },
    clanName: {
        type: String
    },
    isUserVerified: {
        type: Boolean,
        default: false
    },
    isEmailVerified: {
        type: Boolean,
        default: false
    }
}, {
    timestamps: true
});

module.exports = usersSchema;