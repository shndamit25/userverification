# Configuration to change
replace twilio keys(TWILIO_ACCOUNT_SID,TWILIO_AUTH_TOKEN,VERIFICATION_SID) in properties.js
replace clan name values in constant.js
replace DB in properties.js with cloud Connection String(can refer steps to generate mongodb connection in MongoSetup.odt) 
replace captcha keys in properties.js(can refer steps for captcha configuration in CaptchaSetup.odt)


# Steps to run
Navigate to project folder and click npm install
Enter node server.js
Open http://localhost:4000/
