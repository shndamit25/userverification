var express = require("express");
var bodyParser = require("body-parser");
var Users = require('./api/user/users.service');
var clanNames = require('./api/user/constant').clanNames
const { check, validationResult } = require('express-validator');
var Configurations = require('./config/properties.js');
var cookieParser = require('cookie-parser');
var session = require('express-session');

var db = require('./config/database');
db();
var app = express()
app.set("view engine", "ejs");
app.use(bodyParser.json());
app.use(express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(cookieParser());
app.use(session({secret: "Its a secret!",  resave: false,
saveUninitialized: true}));


app.get('/', function (req, res) {
    res.set({
        'Access-control-Allow-Origin': '*'
    });
    return res.render('index', { user:{}, validationErrors: {}, clanNames,captcha_sitekey:Configurations.CAPTCHA_SITE_KEY});
}).listen(4000)

app.get('/sign_up',function(req,res){
    return res.render('index',{user:req.session.userData,validationErrors: req.session.validationMessages, clanNames,captcha_sitekey:Configurations.CAPTCHA_SITE_KEY})
})

app.get('/emailVerified',function(req,res){
    if(!req.session.userData) {
        return res.redirect('/')
    }
    Users.isEmailVerified(req.session.userData.email).then((isVerified)=>{
        if(!isVerified) {
            return res.redirect('/')
        }
        return res.render('emailVerified')
    }).catch((err)=>{
        console.log(err)
        return res.render('error');
    })
})

app.get('/verifyEmail',function(req,res){
    if(!req.session.userData) {
        return res.redirect('/')
    }
    return res.render('verifyEmail', { OTPMessage: req.session.OTPMessage })

})

app.get('/verifyMobile',function(req,res){
    if(!req.session.userData) {
        return res.redirect('/')
    }
    Users.isEmailVerified(req.session.userData.email).then((isVerified)=>{
        if(!isVerified) {
            return res.redirect('/')
        }
        return res.render('verifyMobile', { OTPMessage: req.session.OTPMessage })
    }).catch((err)=>{
        console.log(err)
        return res.render('error');
    })
})

app.post('/sign_up', [
    check('firstName').not().isEmpty(),
    check('lastName').not().isEmpty(),
    check('email').isEmail()
], async (req, res) => {
    try {
        req.session.userData = req.body;
        req.session.validationMessages = {};
        req.session.OTPMessage = '';
        let isCaptchaValid = await Users.verifyCaptcha(req);
        if(!isCaptchaValid) {
            req.session.validationMessages['captcha'] = 'Please Verify Captcha';
            return res.redirect('/sign_up');
        }
        let isMobileValid = await Users.checkIfValidPhoneNumber(req.body.mobile)
        let validationData = validationResult(req);
        if (!isMobileValid) {
            validationData.errors.push({
                param: 'mobile',
                value: req.body.mobile
            })
        }
        if (req.body.mobile != req.body.mobileConfirm) {
            validationData.errors.push({
                param: 'mobileConfirm',
                value: req.body.mobileConfirm
            })
        }
        if (req.body.email != req.body.emailConfirm) {
            validationData.errors.push({
                param: 'emailConfirm',
                value: req.body.emailConfirm
            })
        }
        if (validationData.errors.length) {
            req.session.validationMessages = Users.createValidationMessages(validationData.errors)
            return res.redirect('/sign_up');
        }
        req.session.validationMessages = await Users.createUser(req.body)
        if (req.session.validationMessages != 'User created successfully') {
            return res.redirect('/sign_up');
        }
        await Users.sendOTP(req.body.email, 'email')
        return res.redirect('/verifyEmail');
    } catch (err) {
        console.log(err)
        return res.render('error');
    }
})

app.post('/verifyEmail', [
    check('verificationCode').isNumeric(),
    check('verificationCode').isLength({ min: 4, max:10 })
], async (req, res) => {
    try {
        const validationData = validationResult(req);
        if (validationData.errors.length) {
            req.session.OTPMessage = 'Enter valid Verification Code';
            return res.redirect('/verifyEmail');
        }
        let OTPMessage = await Users.verify(req.body.verificationCode, req.session.userData.email)
        if (OTPMessage == 'Enter correct verification code') {
           req.session.OTPMessage = 'Enter correct verification code';
            return res.redirect('/verifyEmail');
        }
        await Users.sendOTP('+' + req.session.userData.mobile, 'sms')
        await Users.modifyEmailVerification(req.session.userData.email)
        req.session.OTPMessage = '';
        return res.redirect('/verifyMobile');
    } catch (err) {
        console.log(err)
        return res.render('error');
    }
})

app.post('/verifyMobile', [
    check('verificationCode').isNumeric(),
    check('verificationCode').isLength({ min: 4, max:10 })
], async (req, res) => {
    try {
        const validationData = validationResult(req);
        if (validationData.errors.length) {
            req.session.OTPMessage = 'Enter valid Verification Code';
            return res.redirect('/verifyMobile');
        }
        let OTPMessage = await Users.verify(req.body.verificationCode, '+' + req.session.userData.mobile)
        if (OTPMessage == 'Enter correct verification code') {
           req.session.OTPMessage = 'Enter correct verification code';
            return res.redirect('/verifyMobile');
        }
        req.session.OTPMessage = '';
        await Users.modifyVerification(req.session.userData)
        return res.render('signup_success');
    } catch (err) {
        console.log(err)
        return res.render('error');
    }
})

app.post('/resendOTPMobile',async(req,res)=>{
    try{
        await Users.sendOTP('+' + req.session.userData.mobile, 'sms')
        return res.redirect('/verifyMobile');
    } catch(err) {
        console.log(err)
        return res.render('error');
    }

})








console.log("server listening at port 4000"); 
